import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import VRuntimeTemplate from "v-runtime-template";
Vue.component('v-runtime-template', VRuntimeTemplate)

import VueResource from 'vue-resource'

Vue.use(VueResource);
Vue.http.options.root = '/';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

router.beforeEach((to, from, next) => {
  // ...
  
  console.log('beforeEach' + to.path);
  Vue.http.get('htmlPage/' + to.name + '.html').then(response => {
    // get body data

    // console.log(response.body);
    // this.someData = response.body;
    // console.log(store.state.pages[to.path])
    if (store.state.pages[to.path] == undefined) {
      store.commit('setPages', {path: to.path, content: response.body});
    }

    next();
  }, () => {
    // console.log(response.body);
    console.log('404');
    // error callback
    next();
  });
})
router.afterEach((to, from, next) => {
  // ...
  
  console.log('afterEach' + to.path);
  // Vue.http.get('htmlPage/' + to.name + '.html').then(response => {
  //   // get body data

  //   console.log(response.body);
  //   // this.someData = response.body;
  //   next();
  // }, response => {
  //   console.log(response.body);
  //   console.log('404');
  //   // error callback
  //   next();
  // });
})