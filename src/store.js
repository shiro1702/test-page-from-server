import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pages: {}
  },
  getters: {
    page: state => path => {
      return state.pages[path];
    }
  },
  mutations: {
    setPages(state, date){
      state.pages[date.path] = date.content;
    },
    delPages(state, date){
      state.pages[date.path] = undefined;
    },
  },
  actions: {

  }
})
